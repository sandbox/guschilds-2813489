<?php

namespace Drupal\simple_inline_responsive_images\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\file\FileInterface;
use Drupal\filter\Annotation\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to render inline images through a responsive image style.
 *
 * @Filter(
 *   id = "filter_responsive_image_style",
 *   module = "simple_inline_responsive_images",
 *   title = @Translation("Display responsive image styles"),
 *   description = @Translation("Renders all inline images through a responsive image style."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class FilterResponsiveImageStyle extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a Drupal\responsive_image\Plugin\Filter\FilterResponsiveImageStyle object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, ImageFactory $image_factory, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->imageFactory = $image_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('entity_type.manager'), $container->get('entity.repository'), $container->get('image.factory'), $container->get('renderer'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $responsive_image_styles = $this->loadResponsiveImageStyles();
    $default_value = !empty($this->settings['responsive_image_style']) ? $this->settings['responsive_image_style'] : NULL;

    $form['responsive_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Responsive image style'),
      '#description' => $this->t('All inline images will be rendered through this image style.'),
      '#options' => $responsive_image_styles,
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (stristr($text, '<img') !== FALSE) {
      // Load all the responsive image styles to ensure the selected responsive
      // image style is valid.
      $responsive_image_styles = $this->loadResponsiveImageStyles();

      // Load the text that is being processed into XML to find images.
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      // Process each img element DOM element found with the necessary
      // attributes.
      /** @var \DOMElement $dom_element */
      foreach ($xpath->query('//*[@data-entity-type="file" and @data-entity-uuid]') as $dom_element) {
        // Get the UUID and responsive image style for the file.
        $file_uuid = $dom_element->getAttribute('data-entity-uuid');
        $responsive_image_style_id = $this->settings['responsive_image_style'];

        // If the responsive image style is not a valid one, then don't
        // transform the HTML.
        if (empty($file_uuid) || !in_array($responsive_image_style_id, array_keys($responsive_image_styles))) {
          continue;
        }

        // Transform the HTML for the img element by applying a responsive image
        // style.
        $altered_img = $this->getResponsiveImageStyleHtml($file_uuid, $responsive_image_style_id, $dom_element);

        // Load the altered HTML into a new DOMDocument and retrieve the element.
        $updated_node = Html::load(trim($altered_img))->getElementsByTagName('body')
          ->item(0)
          ->childNodes
          ->item(0);

        // Import the updated node from the new DOMDocument into the original
        // one, importing also the child nodes of the updated node.
        $updated_node = $dom->importNode($updated_node, TRUE);

        // Finally, replace the original image node with the new image node.
        $dom_element->parentNode->replaceChild($updated_node, $dom_element);
      }

      // Process the filter with the newly updated DOM.
      return new FilterProcessResult(Html::serialize($dom));
    }

    // Process the filter if no image style img elements are found.
    return new FilterProcessResult($text);
  }

  /**
   * Loads the responsive image styles.
   *
   * @return string[]
   */
  protected function loadResponsiveImageStyles() {
    $data = [];

    $responsive_image_styles = $this->entityTypeManager->getStorage('responsive_image_style')->loadMultiple();
    foreach ($responsive_image_styles as $name => $image_style) {
      $data[$name] = $image_style->label();
    }

    return $data;
  }

  /**
   * Get the the width and height of an image based on the file UUID.
   *
   * @param string $file_uuid
   *   The UUID for the file.
   *
   * @return array
   *   The image information.
   */
  protected function getImageInfo($file_uuid) {
    /** @var \Drupal\file\FileInterface $file; */
    $file = $this->entityRepository->loadEntityByUuid('file', $file_uuid);

    // Determine uri, width and height of the source image.
    $image_uri = $image_width = $image_height = NULL;
    $image = $this->imageFactory->get($file->getFileUri());
    if ($image->isValid()) {
      $image_uri = $file->getFileUri();
      $image_width = $image->getWidth();
      $image_height = $image->getHeight();
    }

    return [
      'uri' => $image_uri,
      'width' => $image_width,
      'height' => $image_height
    ];
  }

  /**
   * Removes attributes that will be generated from image style theme function.
   *
   * @param \DOMElement $dom_element
   *   The DOM element for the img element.
   *
   * @return array
   *  The attributes array.
   */
  protected function prepareResponsiveImageAttributes(\DOMElement $dom_element) {
    // Remove attributes that are no longer needed.
    $dom_element->removeAttribute('data-entity-type');
    $dom_element->removeAttribute('data-entity-uuid');

    // Remove attributes that are generated by the image style.
    $dom_element->removeAttribute('width');
    $dom_element->removeAttribute('height');
    $dom_element->removeAttribute('src');

    // Make sure all non-regenerated attributes are retained.
    $attributes = [];
    for ($i = 0; $i < $dom_element->attributes->length; $i++) {
      $attr = $dom_element->attributes->item($i);
      $attributes[$attr->name] = $attr->value;
    }

    return $attributes;
  }

  /**
   * Get the HTML for the img element after image style is applied.
   *
   * @param string $file_uuid
   *   The UUID for the file.
   * @param string $responsive_image_style_id
   *   The ID for the responsive image style.
   * @param \DOMElement $dom_element
   *  The DOM element for the image element.
   *
   * @return string
   *   The img element with the image style applied.
   */
  protected function getResponsiveImageStyleHtml($file_uuid, $responsive_image_style_id, \DOMElement $dom_element) {
    $image_info = $this->getImageInfo($file_uuid);

    // Remove attributes that will be generated by the image style.
    $attributes = $this->prepareResponsiveImageAttributes($dom_element);

    // Ensure the class attribute value is an array to avoid fatal errors.
    if (!empty($attributes['class']) && gettype($attributes['class']) == 'string') {
      $attributes['class'] = [$attributes['class']];
    }

    // Re-render as a responsive image.
    $responsive_image = [
      '#theme' => 'responsive_image',
      '#responsive_image_style_id' => $responsive_image_style_id,
      '#uri' => $image_info['uri'],
      '#width' => $image_info['width'],
      '#height' => $image_info['height'],
      '#attributes' => $attributes,
    ];

    return $this->renderer->render($responsive_image);
  }
}
