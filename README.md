Simple Inline Responsive Images
===

Provides a filter to render inline images through a responsive image style.

This module was originally constructed from code within a core patch in
https://www.drupal.org/node/2061377, specifically the patch from #198. Endless
thanks to everyone who has contributed to that issue thus far, including Wim
Leers, Jelle_S, attiks, and mdrummond.

This module was created from that patch to satisfy the immediate needs of a
client and to include one key difference. That difference is that all inline
images will be rendered through the same responsive image style, rather than
allowing the user to select a separate image style on every inline image. This
satisfies the needs of the client while keeping the editorial UI and this
module's code as simple as possible.

Installation and configuration
---

1. Install as you would normally install a contributed Drupal module.
2. Navigate to the "Text formats and editors" configuration page.
3. Click "Configure" next to the text format that should render inline images
   through a responsive image style.
4. Enable the "Display responsive image styles" filter under "Enabled filters".
5. Under "Filter processing order", ensure the filter comes after other image
   filters, such as those for alignment and captions.
6. Under filter settings for the newly enabled filter, select the responsive
   image style that all inline images should be rendered with.
7. Click "Save configuration" and repeat for any other text formats needed.
